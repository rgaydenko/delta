package delta.managers;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import delta.entities.ParameterName;
import delta.entities.PriceItem;
import delta.repos.PriceItemsRepository;

@Component
public class PriceItemManager {
	
	Map<String,Integer> fieldsOrder;
	
	@Autowired 
	private PriceItemsRepository priceItemsRepository;
	
	@Transactional
	public boolean parse(BufferedReader reader) {
		String s;
		String[] parsedLine;
		int lineNumber = 0;
		
		
		List<PriceItem> priceItems = new ArrayList<>();
		
		List<String> fieldList = ParameterManager.getList(ParameterName.FORMAT);		
		Map<String,Integer> fieldOrder = new HashMap<>();		
		for(String value : fieldList)
			fieldOrder.put(value, fieldList.indexOf(value));
		
  	  try {
		while ((s = reader.readLine())!=null) {	
			  lineNumber++;
			  try {
				  parsedLine = s.split(";");			  
				  PriceItem priceItem = new PriceItem();
				  priceItem.setVendor(parsedLine[fieldOrder.get("Vendor")]);
				  priceItem.setNumber(parsedLine[fieldOrder.get("Number")]);
				  priceItem.setSearchVendor(makeSearchFields(parsedLine[fieldOrder.get("Vendor")]));
				  priceItem.setSearchNumber(makeSearchFields(parsedLine[fieldOrder.get("Number")]));
				  priceItem.setDescription(truncateString(parsedLine[fieldOrder.get("Description")]));
				  priceItem.setPrice(Float.parseFloat(parsedLine[fieldOrder.get("Price")].replace(',', '.')));
				  priceItem.setCount(truncateNumber(parsedLine[fieldOrder.get("Count")]));		
			  
				  priceItems.add(priceItem);
			  } catch (NumberFormatException e) {
				  System.out.println("Line parsing error at line " + lineNumber);
				  e.printStackTrace();
			  }
		  	}
	  	
	  	priceItemsRepository.deleteAll();
	  	priceItemsRepository.saveAll(priceItems);
	  	
	  	return true;
  	  } catch (IOException e) {
		  e.printStackTrace();
  	  } catch (NullPointerException e) {
  		  throw new IllegalArgumentException("Field was not found in application parameter");
  	  }
  	  return false;
	}
	
	String makeSearchFields(String inString) {
		return inString.replaceAll("[^a-zA-Z0-9а-яА-Я]", "").toUpperCase();
	}
	
	String truncateString(String inString) {
		int limit = 512;
		if (inString.length() <= limit)
			return inString;
		else 
			return inString.substring(0, limit);
	}
	
	int truncateNumber(String inString) {
		return Integer.parseInt(inString.substring(inString.lastIndexOf('-')+1, inString.length()).
				replace(">", "").
				replace("<", ""));
	}
}

package delta.managers;

import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import delta.entities.*;
import delta.repos.ParametersRepository;

@Component
public class ParameterManager {
	
	public static Map<ParameterName,String> parameters;
	
	@Autowired
	private ParametersRepository parametersRepository;
	
	@PostConstruct     
	private void initStatic () {
		parameters = parametersRepository.findAll().
				stream().
				collect(Collectors.toMap(Parameter::getName, Parameter::getValue));
	}
		
	public static String getValue(ParameterName name) {
		return parameters.get(name);
	}
	
	public static List<String> getList(ParameterName name) {
		return Arrays.asList(getValue(name).split("\\s*;\\s*"));
	}
	
	public Map<ParameterName,String> getAllProperties() {
		return parameters;
	}
	
	public Parameter getProperty(ParameterName name) {
		return parametersRepository.findByName(name);
	}
	
	public Parameter updateParameter(ParameterName name, String value) {
		Parameter parameter = new Parameter(name, value);
		parametersRepository.save(parameter);
		parameters.put(name, value);
		return parameter;
	}
	
	public Parameter createParameter(ParameterName name, String value) {
		Parameter parameter = new Parameter(name, value);
		parametersRepository.save(parameter);
		parameters.put(name, value);
		return parameter;
	}
		
		
		
}

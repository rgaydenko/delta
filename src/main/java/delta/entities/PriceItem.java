package delta.entities;


import javax.persistence.*;

@Entity
@Table(name = "PRICEITEMS")
public class PriceItem {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(length = 64)
	private String vendor;
	@Column(length = 64)
	private String number;
	@Column(length = 64)
	private String searchVendor;
	@Column(length = 64)
	private String searchNumber;
	@Column(length = 512)
	private String description;
	@Column(precision = 20, scale = 2)
	private float price;
	private int count;
	
	public PriceItem() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	
	
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getSearchVendor() {
		return searchVendor;
	}

	public void setSearchVendor(String searchVendor) {
		this.searchVendor = searchVendor;
	}

	public String getSearchNumber() {
		return searchNumber;
	}

	public void setSearchNumber(String searchNumber) {
		this.searchNumber = searchNumber;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}

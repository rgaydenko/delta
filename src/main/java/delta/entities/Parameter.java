package delta.entities;

import javax.persistence.*;

@Entity
@Table(name = "PARAMETERS")
public class Parameter {
	
	@Id
	private ParameterName name;
	@Column
	private String value;
	
	public Parameter() {
	}
	
	public Parameter(ParameterName name, String value) {
		this.name = name;
		this.value = value;
	}

	public ParameterName getName() {
		return name;
	}
	public void setName(ParameterName name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}	
}

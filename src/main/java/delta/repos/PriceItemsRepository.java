package delta.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import delta.entities.PriceItem;

@Repository
public interface PriceItemsRepository extends CrudRepository<PriceItem, Long> {
	
	public List<PriceItem> findAll();
	
}

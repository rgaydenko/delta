package delta.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import delta.entities.*;

public interface ParametersRepository extends CrudRepository<Parameter,ParameterName>{
	public Parameter findByName(ParameterName name);
	public List<Parameter> findAll();	
}

package delta.controllers;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import delta.entities.PriceItem;
import delta.repos.PriceItemsRepository;

@RestController
public class LoaderRESTController {
	
	@Autowired
	PriceItemsRepository priceItemRepo;
	@Autowired
	MailLoader mailLoader;
	
	@GetMapping("load")
	public String loadList() throws MessagingException, IOException {
		boolean result = mailLoader.getMail();		
		
		return result ? "List was loaded" : "No list was loaded";
	}
	
	@GetMapping("show")
	public List<PriceItem> showPriceItems() {
		return priceItemRepo.findAll();
	}

}

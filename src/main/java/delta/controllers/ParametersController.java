package delta.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import delta.entities.*;
import delta.managers.ParameterManager;

@RestController
@RequestMapping("/parameters")
public class ParametersController {
	
	@Autowired
	ParameterManager parameterManager;
	
	@GetMapping
	public Map<ParameterName,String> getProperties() {
		return parameterManager.getAllProperties();
	}
	
	@GetMapping("{name}")
	public Parameter getProperty(@PathVariable ParameterName name) {
		return parameterManager.getProperty(name);
	}
	
	@PostMapping("/init")
	public String init() {
		parameterManager.createParameter(ParameterName.CONNECT,"imap.gmail.com;test.gaydenko@gmail.com;qazwsx098");
		parameterManager.createParameter(ParameterName.FORMAT,";Vendor;;Description;;;Price;;Count;;Number;;;");
		parameterManager.createParameter(ParameterName.EMAIL,"test.gaydenko@gmail.com");
		
		return "Initialized";
	}
	
	@PutMapping("/update/{name}")
	public Parameter updateParameter(@PathVariable ParameterName name, @RequestParam(name = "value") String value) {
		return parameterManager.updateParameter(name, value);
	}
}	

package delta.controllers;

import javax.mail.*;
import javax.mail.internet.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import delta.entities.ParameterName;
import delta.managers.ParameterManager;
import delta.managers.PriceItemManager;

import java.util.*;
import java.io.*;

@Component
public class MailLoader {
	
	@Autowired
	PriceItemManager priceItemManager;
			
    String provider  = "imap";
    
    public boolean getMail() throws MessagingException, IOException {
    	Store store = null;
    	Folder folder = null;
    	Properties props = new Properties();
    	props.setProperty("mail.store.protocol", "imaps");
    	Session session = Session.getDefaultInstance(props, null);
        try {
			store = session.getStore("imaps");
			List<String> parameters = ParameterManager.getList(ParameterName.CONNECT);
	        store.connect(parameters.get(0),parameters.get(1),parameters.get(2));
	        folder = store.getFolder("Inbox");
	        folder.open(Folder.READ_WRITE);
	        Message messages[] = folder.getMessages();
	        for (int i=0; i < messages.length; ++i) {
	          Message msg = messages[i];
	          
	          /*String from = "unknown";
	          if (msg.getFrom().length >= 1) {
	            from = msg.getFrom()[0].toString();
	          }*/
	          
	          String contentType = msg.getContentType();
	          if (contentType.contains("multipart")) {
	        	  Multipart multiPart = (Multipart) msg.getContent();
	        	  
	        	  for (int j = 0; j < multiPart.getCount(); j++) {
	        	      MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(j);
	        	      if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
	        	    	  String fileExtension = part.getFileName().substring(part.getFileName().lastIndexOf(".")+1);
	        	    	  
	        	    	  if (fileExtension.equals("csv")) {
	        	    	    boolean result = priceItemManager.parse(new BufferedReader(new InputStreamReader(part.getInputStream())));
	        	    	    msg.setFlag(Flags.Flag.DELETED, true);
	        	    	    return result;
	        	    	  }	        	    	  
	        	      }
	        	  }      	  
	        	  
	        	}
	        }
	        return false;
	      }
	      finally {
	        if (folder != null) {folder.close(true);}
	        if (store != null) {store.close();}
	       }
    }
}

package delta.managers;

import static org.junit.Assert.*;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;

public class PriceItemManagerTest {
	PriceItemManager priceItemManager;
	
	@Before
	public void init() {
		priceItemManager = new PriceItemManager();
	}

	@Test
	public void testMakeSearchFields() {
		String result = priceItemManager.makeSearchFields("aBcD-+!@# 123");
		String expected = "ABCD123";
		assertEquals(expected, result);
	}

	@Test
	public void testTruncateString() {
		String result = priceItemManager.truncateString(Stream.generate(() -> "a").limit(600).collect(Collectors.joining()));
		String expected = Stream.generate(() -> "a").limit(512).collect(Collectors.joining());
		assertEquals(expected, result);
	}

	@Test
	public void testTruncateNumberGreaterThen() {
		int result = priceItemManager.truncateNumber(">10");
		int expected = 10;
		assertEquals(expected,result);
	}
	
	@Test
	public void testTruncateNumberRange() {
		int result = priceItemManager.truncateNumber("10-20");
		int expected = 20;
		assertEquals(expected,result);
	}	

}
